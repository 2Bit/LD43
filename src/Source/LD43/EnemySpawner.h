// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "EnemySpawner.generated.h"

UCLASS()
class LD43_API AEnemySpawner : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* Root;

public:
	AEnemySpawner();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawner)
	TSubclassOf<class AEnemyCharacter> EnemyClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawner)
	uint32 bBeginSpawned : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawner)
	uint32 bEnsureSpawned : 1;

	UFUNCTION(BlueprintCallable, Category = Spawner)
	bool IsSpawning() const;

	UFUNCTION(BlueprintCallable, Category = Spawner)
	void BeginSpawn();

protected:
	UFUNCTION()
	void Spawn();

private:
	UPROPERTY()
	float SpawnTimeRemaining;

	UPROPERTY()
	class AEnemyCharacter* SpawnedEnemy;
};
