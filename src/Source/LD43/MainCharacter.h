// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Favour.h"

#include "MainCharacter.generated.h"

UCLASS()
class LD43_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UVoxelAnimationComponent* Animation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* Fist;

	UPROPERTY()
	TArray<class USoundCue*> HitSounds;

	UPROPERTY()
	TArray<class USoundCue*> StepSounds;

	UPROPERTY()
	TArray<class USoundCue*> AttackSounds;

	UPROPERTY()
	TArray<class USoundCue*> AttackHitSounds;

	UPROPERTY()
	class USoundCue* JumpSound;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Score;

	AMainCharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	virtual void OnJumped_Implementation() override;
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;

private:
	static int32 MinFavour;
	static int32 MaxFavour;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Favour, meta = (AllowPrivateAccess = "true"))
	TMap<EFavour, int32> Favours;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float AttackCooldownTime;

	UPROPERTY()
	float LastAttackTime;

	UPROPERTY()
	float ReverseStepProgress;

	UPROPERTY()
	float HitPoints;

	UPROPERTY()
	uint32 bIsDead : 1;

	UPROPERTY()
	TSubclassOf<class UUserWidget> KillWidgetClass;

public:
	UFUNCTION(BlueprintCallable)
	void Kill();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Favour)
	static int32 GetMinFavour();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Favour)
	static int32 GetMaxFavour();

	UFUNCTION(BlueprintCallable, Category = Favour)
	int32 GetFavour(EFavour Favour);

	UFUNCTION(BlueprintCallable, Category = Favour)
	void ModifyFavour(EFavour Favour, int32 Delta);

	UFUNCTION(BlueprintCallable, Category = Favour)
	void IncreaseFavour(EFavour Favour);

	UFUNCTION(BlueprintCallable, Category = Favour)
	void ReduceFavour(EFavour Favour);

	UFUNCTION(BlueprintCallable, Category = Combat)
	bool IsAttackCoolingDown() const;

	UFUNCTION(BlueprintCallable, Category = Combat)
	void Attack();

	UFUNCTION(BlueprintCallable)
	FVector GetFistLocation() const;

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return this->CameraBoom; }
	FORCEINLINE class UCameraComponent* GetCamera() const { return this->Camera; }
};
