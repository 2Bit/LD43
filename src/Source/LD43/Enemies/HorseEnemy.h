// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnemyCharacter.h"

#include "HorseEnemy.generated.h"

UCLASS()
class LD43_API AHorseEnemy : public AEnemyCharacter
{
	GENERATED_BODY()

	UPROPERTY()
	class UAudioComponent* BurnAudio;

public:
	AHorseEnemy();

public:
	virtual void Tick(float DeltaSeconds) override;
	
protected:
	virtual void OnStartAttack_Implementation() override;
	virtual void OnPerformAttack_Implementation() override;
	virtual void OnStopAttack_Implementation() override;

private:
	UPROPERTY()
	float PreviousAttackWeight;
};
