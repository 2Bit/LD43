// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnemyCharacter.h"

#include "BullEnemy.generated.h"

UCLASS()
class LD43_API ABullEnemy : public AEnemyCharacter
{
	GENERATED_BODY()

	UPROPERTY()
	class USoundCue* PerformAttackSound;

public:
	ABullEnemy();
	
protected:
	virtual void OnStartAttack_Implementation() override;
	virtual void OnPerformAttack_Implementation() override;
	virtual void OnStopAttack_Implementation() override;
};
