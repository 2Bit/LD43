// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "HorseEnemy.h"

#include "VoxelComponent.h"
#include "ConstructorHelpers.h"
#include "VoxelAnimationComponent.h"
#include "Voxel.h"
#include "MainCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

AHorseEnemy::AHorseEnemy()
{
	this->BurnAudio = this->CreateDefaultSubobject<UAudioComponent>(TEXT("Burn Audio"));
	this->BurnAudio->SetupAttachment(this->GetAnimation());

	static ConstructorHelpers::FObjectFinder<USoundCue> BurnAudio(TEXT("SoundCue'/Game/Sounds/Sun/fire_loop_Cue.fire_loop_Cue'"));
	if (BurnAudio.Succeeded())
	{
		this->BurnAudio->SetSound(BurnAudio.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> FaceVoxel(TEXT("Voxel'/Game/Meshes/Apollo/full-face/apollo-full-face.apollo-full-face'"));
	if (FaceVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("full-face"), FaceVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> FaceAngryVoxel(TEXT("Voxel'/Game/Meshes/Apollo/full-face-angry/apollo-full-face-angry.apollo-full-face-angry'"));
	if (FaceAngryVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("full-face-angry"), FaceAngryVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound1(TEXT("SoundCue'/Game/Sounds/Sun/sun_hit_01_Cue.sun_hit_01_Cue'"));
	if (HitSound1.Succeeded())
	{
		this->HitSounds.Add(HitSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound2(TEXT("SoundCue'/Game/Sounds/Sun/sun_hit_02_Cue.sun_hit_02_Cue'"));
	if (HitSound2.Succeeded())
	{
		this->HitSounds.Add(HitSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeathSound1(TEXT("SoundCue'/Game/Sounds/Sun/sun_die_02_Cue.sun_die_02_Cue'"));
	if (DeathSound1.Succeeded())
	{
		this->DeathSounds.Add(DeathSound1.Object);
	}

	this->Favour = EFavour::Apollo;

	this->HitPoints = 4;
	this->AttackDelayTime = 2.0f;

	this->AIParams.MaxAttackRange = 400.0f;
	this->AIParams.DefaultMovementSpeed = 300.0f;
}

void AHorseEnemy::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (this->IsAttacking())
	{
		float const AttackProgress = this->GetAttackProgress();

		float const AttackWeight = 1.0f - FMath::Pow(1.0f - FMath::Max(0.0f, AttackProgress * 1.5f - 0.5f), 3.0f);

		FHitResult Hit;
		this->SetActorLocation(
			this->GetActorLocation() + this->GetActorForwardVector() * (AttackWeight - this->PreviousAttackWeight) * 600.0f,
			true,
			&Hit);

		this->PreviousAttackWeight = AttackWeight;

		if (Hit.bBlockingHit)
		{
			AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(Hit.Actor);
			if (PlayerCharacter != nullptr)
			{
				PlayerCharacter->TakeDamage(1.0f, FDamageEvent(), this->GetController(), this);

				this->GetCharacterMovement()->AddImpulse(this->GetActorForwardVector().GetSafeNormal() * 1500.0f);

				this->Kill();
			}
		}
	}
}

void AHorseEnemy::OnStartAttack_Implementation()
{
	Super::OnStartAttack_Implementation();

	this->PreviousAttackWeight = 0.0f;

	this->GetAnimation()->ShowFrame(TEXT("full-face-angry"));
}

void AHorseEnemy::OnPerformAttack_Implementation()
{
	Super::OnPerformAttack_Implementation();
}

void AHorseEnemy::OnStopAttack_Implementation()
{
	Super::OnStopAttack_Implementation();

	this->GetAnimation()->ShowFrame(TEXT("full-face"));
}
