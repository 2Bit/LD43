// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "BirdPoo.h"

#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "EnemyCharacter.h"

ABirdPoo::ABirdPoo()
{
	this->Collider = this->CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
	this->RootComponent = this->Collider;
	this->Collider->InitSphereRadius(30.0f);
	this->Collider->SetCollisionProfileName(TEXT("OverlapAll"));
	this->Collider->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	this->Mesh = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	this->Mesh->SetupAttachment(this->Collider);
	this->Mesh->RelativeScale3D = FVector(0.3f, 0.3f, 0.3f);
	this->Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Meshes/Hermes/feather/feather.feather'"));
	if (Mesh.Succeeded())
	{
		this->Mesh->SetStaticMesh(Mesh.Object);
	}
}

void ABirdPoo::BeginPlay()
{
	Super::BeginPlay();

	this->Collider->SetSimulatePhysics(true);
}

void ABirdPoo::NotifyActorBeginOverlap(AActor* OtherActor)
{
	AEnemyCharacter* const OtherEnemy = Cast<AEnemyCharacter>(OtherActor);
	if (OtherEnemy == nullptr)
	{
		OtherActor->TakeDamage(1.0f, FDamageEvent(), nullptr, this);

		this->Destroy();
	}
}
