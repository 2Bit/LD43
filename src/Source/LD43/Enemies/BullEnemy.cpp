// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "BullEnemy.h"

#include "ConstructorHelpers.h"
#include "VoxelAnimationComponent.h"
#include "Voxel.h"
#include "Engine/World.h"
#include "MainCharacter.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

ABullEnemy::ABullEnemy()
{
	static ConstructorHelpers::FObjectFinder<UVoxel> DefaultVoxel(TEXT("Voxel'/Game/Meshes/Ares/default/ares.ares'"));
	if (DefaultVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("default"), DefaultVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> PreSwingVoxel(TEXT("Voxel'/Game/Meshes/Ares/pre-swing/ares-pre-swing.ares-pre-swing'"));
	if (PreSwingVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("pre-swing"), PreSwingVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> SwingVoxel(TEXT("Voxel'/Game/Meshes/Ares/swing/ares-swing.ares-swing'"));
	if (SwingVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("swing"), SwingVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound1(TEXT("SoundCue'/Game/Sounds/Minotaur/entity_hit_02_Cue.entity_hit_02_Cue'"));
	if (HitSound1.Succeeded())
	{
		this->HitSounds.Add(HitSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound2(TEXT("SoundCue'/Game/Sounds/Player/entity_hit_01_Cue.entity_hit_01_Cue'"));
	if (HitSound2.Succeeded())
	{
		this->HitSounds.Add(HitSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound3(TEXT("SoundCue'/Game/Sounds/Player/entity_hit_03_Cue.entity_hit_03_Cue'"));
	if (HitSound3.Succeeded())
	{
		this->HitSounds.Add(HitSound3.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound4(TEXT("SoundCue'/Game/Sounds/Player/entity_hit_04_Cue.entity_hit_04_Cue'"));
	if (HitSound4.Succeeded())
	{
		this->HitSounds.Add(HitSound4.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeathSound1(TEXT("SoundCue'/Game/Sounds/Minotaur/minotaur_die_Cue.minotaur_die_Cue'"));
	if (DeathSound1.Succeeded())
	{
		this->DeathSounds.Add(DeathSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> PerformAttackSound(TEXT("SoundCue'/Game/Sounds/Minotaur/axe_impact_01_Cue.axe_impact_01_Cue'"));
	if (PerformAttackSound.Succeeded())
	{
		this->PerformAttackSound = PerformAttackSound.Object;
	}

	this->Favour = EFavour::Ares;

	this->HitPoints = 8;
	this->AttackDelayTime = 2.0f;

	this->AIParams.DefaultMovementSpeed = 200.0f;
}

void ABullEnemy::OnStartAttack_Implementation()
{
	Super::OnStartAttack_Implementation();

	this->GetAnimation()->ShowFrame(TEXT("pre-swing"));
}

void ABullEnemy::OnPerformAttack_Implementation()
{
	Super::OnPerformAttack_Implementation();

	TArray<FOverlapResult> Overlaps;
	if (this->GetWorld()->OverlapMultiByChannel(
		Overlaps,
		this->GetActorLocation() + this->GetActorForwardVector() * 100.0f,
		this->GetActorRotation().Quaternion(),
		ECollisionChannel::ECC_Pawn,
		FCollisionShape::MakeCapsule(150.0f, 100.0f)))
	{
		for (int i = 0; i < Overlaps.Num(); ++i)
		{
			AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(Overlaps[i].Actor);
			if (PlayerCharacter != nullptr)
			{
				PlayerCharacter->TakeDamage(1.0f, FDamageEvent(), this->GetController(), this);
			}
		}
	}

	UGameplayStatics::PlaySoundAtLocation(this, this->PerformAttackSound, this->GetActorLocation());
}

void ABullEnemy::OnStopAttack_Implementation()
{
	Super::OnStopAttack_Implementation();

	this->GetAnimation()->ShowFrame(TEXT("swing"));
	this->GetAnimation()->ShowFrame(TEXT("default"), 0.4f);
}
