// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "BirdEnemy.h"

#include "Components/SphereComponent.h"
#include "ConstructorHelpers.h"
#include "VoxelAnimationComponent.h"
#include "Voxel.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Pawn.h"
#include "EnemyAIController.h"
#include "BirdPoo.h"
#include "Sound/SoundCue.h"

ABirdEnemy::ABirdEnemy()
{
	this->Collider = this->CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
	this->Collider->SetupAttachment(this->GetAnimation());
	this->Collider->RelativeLocation = FVector(0.0f, 0.0f, 300.0f);
	this->Collider->InitSphereRadius(150.0f);

	this->GetAnimation()->RelativeRotation += FRotator(0.0f, 180.0f, 0.0f);

	static ConstructorHelpers::FObjectFinder<UVoxel> DefaultVoxel(TEXT("Voxel'/Game/Meshes/Hermes/default/hermes.hermes'"));
	if (DefaultVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("default"), DefaultVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> FlapVoxel(TEXT("Voxel'/Game/Meshes/Hermes/flap/hermes-flap.hermes-flap'"));
	if (FlapVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("flap"), FlapVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> FlapClimaxVoxel(TEXT("Voxel'/Game/Meshes/Hermes/flap-climax/hermes-flap-climax.hermes-flap-climax'"));
	if (FlapClimaxVoxel.Succeeded())
	{
		this->GetAnimation()->AddFrame(TEXT("flap-climax"), FlapClimaxVoxel.Object);
	}

	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	this->GetCapsuleComponent()->SetCanEverAffectNavigation(false);

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound1(TEXT("SoundCue'/Game/Sounds/Bird/bird_hit_01_Cue.bird_hit_01_Cue'"));
	if (HitSound1.Succeeded())
	{
		this->HitSounds.Add(HitSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound2(TEXT("SoundCue'/Game/Sounds/Bird/bird_hit_02_Cue.bird_hit_02_Cue'"));
	if (HitSound2.Succeeded())
	{
		this->HitSounds.Add(HitSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound3(TEXT("SoundCue'/Game/Sounds/Bird/bird_hit_03_Cue.bird_hit_03_Cue'"));
	if (HitSound3.Succeeded())
	{
		this->HitSounds.Add(HitSound3.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound4(TEXT("SoundCue'/Game/Sounds/Bird/bird_hit_04_Cue.bird_hit_04_Cue'"));
	if (HitSound4.Succeeded())
	{
		this->HitSounds.Add(HitSound4.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound5(TEXT("SoundCue'/Game/Sounds/Bird/bird_hit_05_Cue.bird_hit_05_Cue'"));
	if (HitSound5.Succeeded())
	{
		this->HitSounds.Add(HitSound5.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeathSound1(TEXT("SoundCue'/Game/Sounds/Bird/bird_death_01_Cue.bird_death_01_Cue'"));
	if (DeathSound1.Succeeded())
	{
		this->DeathSounds.Add(DeathSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> DeathSound2(TEXT("SoundCue'/Game/Sounds/Bird/bird_death_02_Cue.bird_death_02_Cue'"));
	if (DeathSound2.Succeeded())
	{
		this->DeathSounds.Add(DeathSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StartAttackSound(TEXT("SoundCue'/Game/Sounds/Bird/bird_dive_Cue.bird_dive_Cue'"));
	if (StartAttackSound.Succeeded())
	{
		this->StartAttackSound = StartAttackSound.Object;
	}

	this->Favour = EFavour::Hermes;

	this->HitPoints = 2;
	this->AttackDelayTime = 5.0f;

	this->AIParams.MaxAttackRange = 800.0f;
	this->AIParams.MinAttackAngle = -1.0f;
	this->AIParams.AttackCooldown = 5.0f;
	this->AIParams.WanderChance = 0.8f;
	this->AIParams.CanApproach = false;
	this->AIParams.DefaultMovementSpeed = 300.0f;
}

void ABirdEnemy::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (FMath::Fmod(this->GetWorld()->GetRealTimeSeconds(), 1.0f) >= 0.5f)
	{
		this->GetAnimation()->ShowFrame(TEXT("default"));
	}
	else
	{
		if (this->IsAttacking())
		{
			this->GetAnimation()->ShowFrame(TEXT("flap-climax"));
		}
		else
		{
			this->GetAnimation()->ShowFrame(TEXT("flap"));
		}
	}

	if (this->IsAttacking())
	{
		AEnemyAIController* const AIController = Cast<AEnemyAIController>(this->GetController());
		if (AIController != nullptr)
		{
			AIController->MoveToActor(this->GetWorld()->GetFirstPlayerController()->GetPawn());
		}

		this->GetAnimation()->SetRelativeLocation(FMath::Lerp(this->GetAnimation()->RelativeLocation, FVector(0.0f, 0.0f, 150.0f), DeltaSeconds));
	}
	else
	{
		this->GetAnimation()->SetRelativeLocation(FMath::Lerp(this->GetAnimation()->RelativeLocation, FVector(0.0f, 0.0f, -88.0f), 0.5f * DeltaSeconds));
	}
}

void ABirdEnemy::OnStartAttack_Implementation()
{
	Super::OnStartAttack_Implementation();
}

void ABirdEnemy::OnPerformAttack_Implementation()
{
	Super::OnPerformAttack_Implementation();

	this->GetWorld()->SpawnActor<ABirdPoo>(ABirdPoo::StaticClass(), this->GetAnimation()->GetComponentLocation() + FVector(0.0f, 0.0f, 50.0f), this->GetActorRotation());
}

void ABirdEnemy::OnStopAttack_Implementation()
{
	Super::OnStopAttack_Implementation();

	AEnemyAIController* const AIController = Cast<AEnemyAIController>(this->GetController());
	if (AIController != nullptr)
	{
		AIController->IsWandering = true;
	}
}
