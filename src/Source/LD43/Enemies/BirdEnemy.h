// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnemyCharacter.h"

#include "BirdEnemy.generated.h"

UCLASS()
class LD43_API ABirdEnemy : public AEnemyCharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* Collider;

public:
	ABirdEnemy();

	virtual void Tick(float DeltaSeconds) override;
	
protected:
	virtual void OnStartAttack_Implementation() override;
	virtual void OnPerformAttack_Implementation() override;
	virtual void OnStopAttack_Implementation() override;
};
