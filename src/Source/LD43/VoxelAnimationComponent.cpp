// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "VoxelAnimationComponent.h"

#include "ConstructorHelpers.h"
#include "Particles/ParticleSystem.h"
#include "VoxelComponent.h"
#include "Voxel.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

UVoxelAnimationComponent::UVoxelAnimationComponent()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ChunkParticleSystem(TEXT("ParticleSystem'/Game/Particles/Cube/PS_Cube.PS_Cube'"));
	if (ChunkParticleSystem.Succeeded())
	{
		this->ChunkParticleSystem = ChunkParticleSystem.Object;
	}

	this->bIsRunning = false;
	this->DestroyIndex = -1;

	this->PrimaryComponentTick.bCanEverTick = true;
}

void UVoxelAnimationComponent::BeginPlay()
{
	Super::BeginPlay();

	this->bIsRunning = true;

	this->FeedFrames();
}

void UVoxelAnimationComponent::TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);

	this->FeedFrames();

	if (this->DelayedFrameTime > 0.0f)
	{
		this->DelayedFrameTime -= DeltaSeconds;

		if (this->DelayedFrameTime <= 0.0f)
		{
			this->ShowFrame(this->DelayedFrameName);
		}
	}

	if (this->DestroyIndex >= 0 && this->CurrentFrame != nullptr)
	{
		float const DestroyRate = this->Keys.Num() / 0.5f;

		for (int i = 0; i < (int)(DestroyRate * DeltaSeconds + 0.5f); ++i)
		{
			if (this->DestroyIndex < this->Keys.Num())
			{
				this->DestroyVoxel(this->CurrentFrame, this->Keys[this->DestroyIndex], FMath::SRand() <= 10000.0f / DestroyRate * DeltaSeconds);

				++this->DestroyIndex;
			}
			else
			{
				this->DestroyIndex = -1;

				break;
			}
		}
	}
}

void UVoxelAnimationComponent::SetMaterial(int32 ElementIndex, UMaterialInterface* Material)
{
	auto Iterator = this->Frames.CreateIterator();
	while (Iterator)
	{
		Iterator.Value()->SetMaterial(ElementIndex, Material);

		++Iterator;
	}
}

void UVoxelAnimationComponent::DestroyRandomChunk(int32 Size)
{
	auto Iterator = this->Frames.CreateIterator();

	FIntVector VisibleVoxel = Iterator.Value()->GetRandomVisibleVoxel();

	while (Iterator)
	{
		UVoxelComponent* Frame = Iterator.Value();

		TArray<FIntVector> ChunkIndicies = Frame->GetSurroundingVoxels(VisibleVoxel, Size);
		for (int i = 0; i < ChunkIndicies.Num(); ++i)
		{
			FIntVector const ChunkIndex = ChunkIndicies[i];

			this->DestroyVoxel(Frame, ChunkIndex, (i % 5) == 0 && Frame == this->CurrentFrame);
		}

		++Iterator;
	}
}

void UVoxelAnimationComponent::DestroyChunk(FVector const& RayOrigin, FVector const& RayDirection, int32 Size)
{
	auto Iterator = this->Frames.CreateIterator();
	while (Iterator)
	{
		UVoxelComponent* Frame = Iterator.Value();

		FIntVector OutIndex;
		if (Frame->FindIntersectingVoxel(RayOrigin, RayDirection, OutIndex, 5))
		{
			TArray<FIntVector> ChunkIndicies = Frame->GetSurroundingVoxels(OutIndex, Size);
			for (int i = 0; i < ChunkIndicies.Num(); ++i)
			{
				FIntVector const ChunkIndex = ChunkIndicies[i];

				this->DestroyVoxel(Frame, ChunkIndex, (i % 5) == 0 && Frame == this->CurrentFrame);
			}
		}

		++Iterator;
	}
}

void UVoxelAnimationComponent::DestroyVoxel(UVoxelComponent* Frame, FIntVector const& VoxelIndex, bool PlayEffect)
{
	if (Frame->HideVoxel(VoxelIndex) && PlayEffect)
	{
		FTransform ChunkIndexTransform;
		Frame->GetVoxelTransform(VoxelIndex, ChunkIndexTransform, true);

		UParticleSystemComponent* ParticlesComponent = UGameplayStatics::SpawnEmitterAtLocation(
			this->GetWorld(),
			this->ChunkParticleSystem, ChunkIndexTransform);

		ParticlesComponent->SetCastShadow(false);

		FColor ChunkColor = Frame->GetVoxelVertexColor(VoxelIndex);
		ParticlesComponent->SetColorParameter(TEXT("ParticleColor"), ChunkColor);
	}
}

void UVoxelAnimationComponent::DestroyAll()
{
	if (!this->IsDestroying())
	{
		this->DestroyIndex = 0;

		this->CurrentFrame->VoxelIdToInstanceId.GetKeys(Keys);
	}
}

bool UVoxelAnimationComponent::IsDestroying() const
{
	return this->DestroyIndex >= 0;
}

bool UVoxelAnimationComponent::AddFrame(FName Name, UVoxel* Frame)
{
	if (this->bIsRunning)
	{
		if (!this->Frames.Contains(Name))
		{
			UVoxelComponent* const NewFrame = NewObject<UVoxelComponent>(this->GetOwner(), Name);
			NewFrame->SetVoxel(Frame);
			NewFrame->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			NewFrame->SetupAttachment(this);
			NewFrame->bHideUnbeheld = false;
			NewFrame->RegisterComponent();
			NewFrame->SetHiddenInGame(true, true);

			this->Frames.Add(Name, NewFrame);

			if (this->Frames.Num() == 1)
			{
				this->ShowFrame(Name);
			}

			return true;
		}
	}
	else
	{
		if (!this->FrameFeed.Contains(Name))
		{
			this->FrameFeed.Add(Name, Frame);

			return true;
		}
	}

	return false;
}

bool UVoxelAnimationComponent::ShowFrame(FName Name, float Delay)
{
	if (this->IsDestroying())
	{
		return false;
	}

	if (Delay > 0.0f)
	{
		this->DelayedFrameName = Name;
		this->DelayedFrameTime = Delay;

		return true;
	}
	else
	{
		this->DelayedFrameTime = 0.0f;

		UVoxelComponent** const NewFramePointer = this->Frames.Find(Name);
		if (NewFramePointer != nullptr && *NewFramePointer != this->CurrentFrame)
		{
			if (this->CurrentFrame != nullptr)
			{
				this->CurrentFrame->SetHiddenInGame(true, true);
			}

			this->CurrentFrame = *NewFramePointer;
			this->CurrentFrame->SetHiddenInGame(false, true);

			return true;
		}
	}

	return false;
}

UVoxelComponent * UVoxelAnimationComponent::GetCurrentFrame()
{
    return this->CurrentFrame;
}

void UVoxelAnimationComponent::FeedFrames()
{
	if (this->FrameFeed.Num() > 0)
	{
		auto Iterator = this->FrameFeed.CreateIterator();
		while (Iterator)
		{
			this->AddFrame(Iterator.Key(), Iterator.Value());

			++Iterator;
		}

		this->FrameFeed.Empty();
	}
}
