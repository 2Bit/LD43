// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainGameMode.h"

#include "MainCharacter.h"
#include "MainPlayerController.h"

AMainGameMode::AMainGameMode()
{
	this->DefaultPawnClass = AMainCharacter::StaticClass();
	this->PlayerControllerClass = AMainPlayerController::StaticClass();
}
