// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Favour.h"

#include "EnemyCharacter.generated.h"

USTRUCT(BlueprintType)
struct FEnemyAIParams
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float AggroRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float MaxAttackRange;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float MinAttackAngle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float AttackCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	uint32 CanAggro : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	uint32 RequireSight : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float WanderChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	uint32 CanWander : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	uint32 CanApproach : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float StepSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float DefaultMovementSpeed;

	FEnemyAIParams()
	{
		this->AggroRange = 1000.0f;
		this->MaxAttackRange = 100.0f;
		this->MinAttackAngle = 0.95f;
		this->AttackCooldown = 1.0f;

		this->CanAggro = true;
		this->RequireSight = true;

		this->WanderChance = 0.2f;

		this->CanWander = true;
		this->CanApproach = true;

		this->StepSpeed = 1.0f;
		this->DefaultMovementSpeed = 300.0f;
	}
};

UCLASS()
class LD43_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UVoxelAnimationComponent* Animation;

protected:
	UPROPERTY()
	TArray<class USoundCue*> HitSounds;

	UPROPERTY()
	TArray<class USoundCue*> DeathSounds;

	UPROPERTY()
	class USoundCue* StartAttackSound;

public:
	AEnemyCharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	FEnemyAIParams AIParams;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Favour)
	EFavour Favour;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
	int32 HitPoints;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
	float AttackDelayTime;

public:
	UFUNCTION(BlueprintCallable, Category = Combat)
	void Kill();

	UFUNCTION(BlueprintCallable, Category = Combat)
	bool IsAttacking() const;

	UFUNCTION(BlueprintCallable, Category = Combat)
	float GetAttackProgress() const;

	UFUNCTION(BlueprintCallable, Category = Combat)
	void StartAttack();

	UFUNCTION(BlueprintCallable, Category = Combat)
	void StopAttack();

protected:
	UFUNCTION(BlueprintNativeEvent, Category = Combat)
	void OnStartAttack();

	UFUNCTION(BlueprintNativeEvent, Category = Combat)
	void OnPerformAttack();

	UFUNCTION(BlueprintNativeEvent, Category = Combat)
	void OnStopAttack();

	virtual void OnStartAttack_Implementation();
	virtual void OnPerformAttack_Implementation();
	virtual void OnStopAttack_Implementation();

private:
	UPROPERTY()
	float AttackDelayTimeRemaining;

	UPROPERTY()
	class UMaterialInterface* MaterialBase;

	UPROPERTY()
	class UMaterialInstanceDynamic* Material;

	UPROPERTY()
	float MaterialFlashRemaining;

public:
	FORCEINLINE class UVoxelAnimationComponent* GetAnimation() const { return this->Animation; }
	FORCEINLINE EFavour GetFavour() const { return this->Favour; }
};
