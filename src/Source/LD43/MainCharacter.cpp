// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainCharacter.h"

#include "ConstructorHelpers.h"
#include "VoxelAnimationComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Voxel.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "EnemyCharacter.h"
#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

int32 AMainCharacter::MinFavour = 1;
int32 AMainCharacter::MaxFavour = 5;

AMainCharacter::AMainCharacter()
{
	this->Animation = this->CreateDefaultSubobject<UVoxelAnimationComponent>(TEXT("Animation"));
	this->Animation->SetupAttachment(this->GetMesh());
	this->Animation->RelativeLocation = FVector(0.0f, 0.0f, -88.0f);
	this->Animation->RelativeRotation = FRotator(0.0f, 180.0f, 0.0f);
	this->Animation->RelativeScale3D = FVector(0.3f, 0.3f, 0.3f);

	this->Fist = this->CreateDefaultSubobject<USceneComponent>(TEXT("Fist"));
	this->Fist->SetupAttachment(this->Animation);
	this->Fist->RelativeLocation = FVector(0.0f, 130.0f, 300.0f);

	static ConstructorHelpers::FObjectFinder<UVoxel> DefaultVoxel(TEXT("Voxel'/Game/Meshes/Zeus/default/zeus.zeus'"));
	if (DefaultVoxel.Succeeded())
	{
		this->Animation->AddFrame(TEXT("default"), DefaultVoxel.Object);
	}

	static ConstructorHelpers::FObjectFinder<UVoxel> PunchVoxel(TEXT("Voxel'/Game/Meshes/Zeus/punch/zeus-punch.zeus-punch'"));
	if (PunchVoxel.Succeeded())
	{
		this->Animation->AddFrame(TEXT("punch"), PunchVoxel.Object);
	}

	this->CameraBoom = this->CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	this->CameraBoom->SetupAttachment(this->RootComponent);
	this->CameraBoom->RelativeLocation = FVector(0.0f, 0.0f, 50.0f);
	this->CameraBoom->RelativeRotation = FRotator(-20.0f, 0.0f, 0.0f);
	this->CameraBoom->TargetArmLength = 200.0f;
	this->CameraBoom->bEnableCameraLag = true;
	this->CameraBoom->CameraLagSpeed = 90.0f;
	
	this->Camera = this->CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	this->Camera->SetupAttachment(this->CameraBoom);

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound1(TEXT("SoundCue'/Game/Sounds/Player/player_hit_01_Cue.player_hit_01_Cue'"));
	if (HitSound1.Succeeded())
	{
		this->HitSounds.Add(HitSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> HitSound2(TEXT("SoundCue'/Game/Sounds/Player/player_hit_02_Cue.player_hit_02_Cue'"));
	if (HitSound2.Succeeded())
	{
		this->HitSounds.Add(HitSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StepSound1(TEXT("SoundCue'/Game/Sounds/Player/player_move_01_Cue.player_move_01_Cue'"));
	if (StepSound1.Succeeded())
	{
		this->StepSounds.Add(StepSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StepSound2(TEXT("SoundCue'/Game/Sounds/Player/player_move_02_Cue.player_move_02_Cue'"));
	if (StepSound2.Succeeded())
	{
		this->StepSounds.Add(StepSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StepSound3(TEXT("SoundCue'/Game/Sounds/Player/player_move_03_Cue.player_move_03_Cue'"));
	if (StepSound3.Succeeded())
	{
		this->StepSounds.Add(StepSound3.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StepSound4(TEXT("SoundCue'/Game/Sounds/Player/player_move_04_Cue.player_move_04_Cue'"));
	if (StepSound4.Succeeded())
	{
		this->StepSounds.Add(StepSound4.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> StepSound5(TEXT("SoundCue'/Game/Sounds/Player/player_move_05_Cue.player_move_05_Cue'"));
	if (StepSound5.Succeeded())
	{
		this->StepSounds.Add(StepSound5.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSound1(TEXT("SoundCue'/Game/Sounds/Player/player_punch_01_Cue.player_punch_01_Cue'"));
	if (AttackSound1.Succeeded())
	{
		this->AttackSounds.Add(AttackSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSound2(TEXT("SoundCue'/Game/Sounds/Player/player_punch_02_Cue.player_punch_02_Cue'"));
	if (AttackSound2.Succeeded())
	{
		this->AttackSounds.Add(AttackSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSound3(TEXT("SoundCue'/Game/Sounds/Player/player_punch_03_Cue.player_punch_03_Cue'"));
	if (AttackSound3.Succeeded())
	{
		this->AttackSounds.Add(AttackSound3.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackSound4(TEXT("SoundCue'/Game/Sounds/Player/player_punch_04_Cue.player_punch_04_Cue'"));
	if (AttackSound4.Succeeded())
	{
		this->AttackSounds.Add(AttackSound4.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackHitSound1(TEXT("SoundCue'/Game/Sounds/Player/player_punch_hit_01_Cue.player_punch_hit_01_Cue'"));
	if (AttackHitSound1.Succeeded())
	{
		this->AttackHitSounds.Add(AttackHitSound1.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackHitSound2(TEXT("SoundCue'/Game/Sounds/Player/player_punch_hit_02_Cue.player_punch_hit_02_Cue'"));
	if (AttackHitSound2.Succeeded())
	{
		this->AttackHitSounds.Add(AttackHitSound2.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> AttackHitSound3(TEXT("SoundCue'/Game/Sounds/Player/player_punch_hit_03_Cue.player_punch_hit_03_Cue'"));
	if (AttackHitSound3.Succeeded())
	{
		this->AttackHitSounds.Add(AttackHitSound3.Object);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> JumpSound(TEXT("SoundCue'/Game/Sounds/Player/player_jump_Cue.player_jump_Cue'"));
	if (JumpSound.Succeeded())
	{
		this->JumpSound = JumpSound.Object;
	}

	this->GetCharacterMovement()->AirControl = 0.5f;

	this->AttackCooldownTime = 0.3f;

	this->HitPoints = 3.0f;

	static ConstructorHelpers::FClassFinder<UUserWidget> KillWidgetClass(TEXT("/Game/UI/BP_DeathWidget.BP_DeathWidget_C"));
	if (KillWidgetClass.Succeeded())
	{
		this->KillWidgetClass = KillWidgetClass.Class;
	}

	this->PrimaryActorTick.bCanEverTick = true;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->ModifyFavour(EFavour::Hermes, 1);
	this->ModifyFavour(EFavour::Ares, 1);
	this->ModifyFavour(EFavour::Apollo, 1);
}

void AMainCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float const VelocitySpeed = this->GetVelocity().Size();

	if (this->ReverseStepProgress > 0.0f)
	{
		this->ReverseStepProgress -= (FMath::Max(300.0f, VelocitySpeed) / 150.0f) * DeltaSeconds;
	}

	if (this->ReverseStepProgress <= 0.0f)
	{
		if (VelocitySpeed > 10.0f && this->GetCharacterMovement()->IsMovingOnGround())
		{
			this->ReverseStepProgress += 1.0f;

			int32 const StepSoundsIndex = FMath::RandRange(0, this->StepSounds.Num() - 1);
			UGameplayStatics::PlaySoundAtLocation(this, this->StepSounds[StepSoundsIndex], this->GetActorLocation(), 0.1f);
		}
		else
		{
			this->ReverseStepProgress = 0.0f;
		}
	}

	float const StepPower = 3.0f;
	float const StepWeight = FMath::Pow(1.0f - (FMath::Abs((1.0f - ReverseStepProgress) - 0.5f) * 2.0f), StepPower);

	this->GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, 10.0f * StepWeight));
}

float AMainCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	this->HitPoints -= DamageAmount;

	//FVector const RayDirection = FVector(FMath::RandRange(-1.0f, 1.0f), FMath::RandRange(-1.0f, 1.0f), FMath::RandRange(0.3f, 1.0f)).GetSafeNormal() * 300.0f;

	//this->Animation->DestroyChunk(this->GetActorLocation() + RayDirection, -RayDirection, 5);

	this->Animation->DestroyRandomChunk(10);

	if (this->HitPoints <= 0.0f)
	{
		this->Kill();
	}
	else
	{
		int32 const HitSoundsIndex = FMath::RandRange(0, this->HitSounds.Num() - 1);
		UGameplayStatics::PlaySoundAtLocation(this, this->HitSounds[HitSoundsIndex], this->GetActorLocation(), 3.0f);
	}

	return DamageAmount;
}

void AMainCharacter::OnJumped_Implementation()
{
	Super::OnJumped_Implementation();

	UGameplayStatics::PlaySoundAtLocation(this, this->JumpSound, this->GetActorLocation());
}

void AMainCharacter::FellOutOfWorld(const class UDamageType& dmgType)
{
	this->Kill();
}

void AMainCharacter::Kill()
{
	AMainPlayerController* const PlayerController = Cast<AMainPlayerController>(this->GetController());
	if (PlayerController != nullptr)
	{
		if (!this->bIsDead)
		{
			this->bIsDead = true;

			this->GetCharacterMovement()->StopMovementImmediately();
			this->GetCharacterMovement()->ClearAccumulatedForces();
			this->GetCharacterMovement()->Velocity = FVector::ZeroVector;

			UUserWidget* const Widget = CreateWidget(PlayerController, this->KillWidgetClass);
			Widget->AddToViewport();
			Widget->SetUserFocus(PlayerController);

			PlayerController->SetInputMode(FInputModeUIOnly());
			PlayerController->bShowMouseCursor = true;

			this->Animation->DestroyAll();
		}
	}
}

int32 AMainCharacter::GetMinFavour()
{
	return AMainCharacter::MinFavour;
}

int32 AMainCharacter::GetMaxFavour()
{
	return AMainCharacter::MaxFavour;
}

int32 AMainCharacter::GetFavour(EFavour Favour)
{
	int32* const CurrentFavour = this->Favours.Find(Favour);
	if (CurrentFavour != nullptr)
	{
		return *CurrentFavour;
	}

	return 0;
}

void AMainCharacter::ModifyFavour(EFavour Favour, int32 Delta)
{
	int32 const CurrentFavour = this->Favours.FindOrAdd(Favour);
	int32 const NewFavour = FMath::Clamp(CurrentFavour + Delta, AMainCharacter::GetMinFavour(), AMainCharacter::GetMaxFavour());

	this->Favours[Favour] = NewFavour;

	switch (Favour)
	{
	case EFavour::Hermes:
		this->GetCharacterMovement()->JumpZVelocity = 400.0f + 100.0f * NewFavour;

		break;
	case EFavour::Ares:
		// Used as attack damage

		break;
	case EFavour::Apollo:
		this->GetCharacterMovement()->MaxWalkSpeed = 400.0f + 100.0f * NewFavour;

		break;
	}
}

void AMainCharacter::IncreaseFavour(EFavour Favour)
{
	this->ModifyFavour(Favour, 1);
}

void AMainCharacter::ReduceFavour(EFavour Favour)
{
	this->ModifyFavour(Favour, -1);
}

bool AMainCharacter::IsAttackCoolingDown() const
{
	return this->GetWorld()->GetRealTimeSeconds() < this->LastAttackTime + this->AttackCooldownTime;
}

void AMainCharacter::Attack()
{
	if (!this->IsAttackCoolingDown())
	{
		this->Animation->ShowFrame(TEXT("punch"));
		this->Animation->ShowFrame(TEXT("default"), 0.2f);

		int32 const AttackSoundsIndex = FMath::RandRange(0, this->AttackSounds.Num() - 1);
		UGameplayStatics::PlaySoundAtLocation(this, this->AttackSounds[AttackSoundsIndex], this->GetActorLocation());

		bool DidAttackHit = false;

		TArray<FOverlapResult> Overlaps;
		if (this->GetWorld()->OverlapMultiByChannel(
			Overlaps,
			this->GetActorLocation() + this->GetActorForwardVector() * 100.0f,
			this->GetActorRotation().Quaternion(),
			ECollisionChannel::ECC_Pawn,
			FCollisionShape::MakeCapsule(75.0f, 50.0f)))
		{
			for (int i = 0; i < Overlaps.Num(); ++i)
			{
				AEnemyCharacter* const OverlapCharacter = Cast<AEnemyCharacter>(Overlaps[i].Actor);
				if (OverlapCharacter != nullptr)
				{
					OverlapCharacter->TakeDamage(this->GetFavour(EFavour::Ares), FDamageEvent(), this->GetController(), this);

					DidAttackHit = true;
				}
			}
		}

		if (DidAttackHit)
		{
			int32 const AttackHitSoundsIndex = FMath::RandRange(0, this->AttackHitSounds.Num() - 1);
			UGameplayStatics::PlaySoundAtLocation(this, this->AttackHitSounds[AttackHitSoundsIndex], this->GetActorLocation());
		}

		this->LastAttackTime = this->GetWorld()->GetRealTimeSeconds();
	}
}

FVector AMainCharacter::GetFistLocation() const
{
	return this->Fist->GetComponentLocation();
}
