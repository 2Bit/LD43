// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Favour.generated.h"

UENUM(BlueprintType)
enum class EFavour : uint8
{
	None = 0,
	Hermes = 1,
	Ares = 2,
	Apollo = 3,
};
