// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "EnemyAIController.h"

#include "EnemyCharacter.h"
#include "VoxelAnimationComponent.h"
#include "VoxelComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "MainCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AEnemyAIController::AEnemyAIController()
{
	this->SpawnTimeRemaining = 1.6f;

	this->PrimaryActorTick.bCanEverTick = true;
}

void AEnemyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (this->SpawnTimeRemaining > 0.0f)
	{
		this->SpawnTimeRemaining -= DeltaSeconds;

		if (this->SpawnTimeRemaining > 0.0f)
		{
			this->GetPawn()->SetActorTickEnabled(false);

			AEnemyCharacter* const ControlledCharacter = Cast<AEnemyCharacter>(this->GetPawn());
			if (ControlledCharacter != nullptr)
			{
				ControlledCharacter->GetAnimation()->GetCurrentFrame()->UpdateVoxelEffects(DeltaSeconds);
			}

			return;
		}
		else
		{
            if (this->SpawnTimeRemaining + DeltaSeconds > 0.0f) {
                AEnemyCharacter* const ControlledCharacter = Cast<AEnemyCharacter>(this->GetPawn());
                if (ControlledCharacter != nullptr)
                {
                    ControlledCharacter->GetAnimation()->GetCurrentFrame()->ShowVoxels();
                }
            }

			this->GetPawn()->SetActorTickEnabled(true);
		}
	}
	
	if (this->ResetLocation == FVector::ZeroVector)
	{
		this->ResetLocation = this->GetPawn()->GetActorLocation();
		this->ResetRotation = this->GetPawn()->GetActorRotation();
	}

	if (this->AggroLingerTimeRemaining > 0.0f)
	{
		this->AggroLingerTimeRemaining -= DeltaSeconds;
	}

	if (this->WanderLingerTimeRemaining > 0.0f)
	{
		this->WanderLingerTimeRemaining -= DeltaSeconds;
	}

	AEnemyCharacter* const ControlledCharacter = Cast<AEnemyCharacter>(this->GetPawn());
	if (ControlledCharacter != nullptr)
	{
		if (!ControlledCharacter->IsAttacking() && this->AttackCooldownTimeRemaining > 0.0f)
		{
			this->AttackCooldownTimeRemaining -= DeltaSeconds;
		}

		FEnemyAIParams const Params = ControlledCharacter->AIParams;

		APlayerController* const PlayerController = this->GetWorld()->GetFirstPlayerController();
		if (PlayerController != nullptr)
		{
			AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(PlayerController->GetPawn());
			if (PlayerCharacter != nullptr)
			{
				// Actions
				bool ShouldReset = false;
				bool ShouldAttack = false;
				bool ShouldWander = false;
				bool ShouldApproach = false;

				// States
				FVector const Displacement = (PlayerCharacter->GetActorLocation() - ControlledCharacter->GetActorLocation()) * FVector(1.0f, 1.0f, 0.0f);

				float const DistanceSqr = Displacement.SizeSquared();
				bool const IsInAggroRange = DistanceSqr <= Params.AggroRange * Params.AggroRange;
				bool const IsInAttackRange = DistanceSqr <= Params.MaxAttackRange * Params.MaxAttackRange;

				bool const IsInSight = !Params.RequireSight || this->LineOfSightTo(PlayerCharacter);

				bool const IsInFront = FVector::DotProduct(Displacement.GetSafeNormal(), ControlledCharacter->GetActorForwardVector()) > Params.MinAttackAngle;

				bool const IsAggroed = Params.CanAggro && IsInAggroRange && IsInSight;

				bool const WasWandering = this->IsWandering;

				// Logic
				if (this->IsWandering)
				{
					ShouldWander = true;

					this->IsWandering = false;
				}

				if (FMath::SRand() <= Params.WanderChance * DeltaSeconds)
				{
					ShouldWander = true;
				}
				
				if (IsAggroed || this->AggroLingerTimeRemaining > 0.0f)
				{
					if (IsAggroed)
					{
						this->AggroLingerTimeRemaining = FMath::RandRange(1.5f, 2.5f);
					}

					if (IsInAttackRange && IsInFront && this->AttackCooldownTimeRemaining <= 0.0f)
					{
						ShouldAttack = true;
					}

					ShouldApproach = true;
				}
				else
				{
					ShouldReset = true;
				}

				// Result
				float MovementSpeed = 1.0f;

				if (!ControlledCharacter->IsAttacking())
				{
					if (ShouldReset)
					{
						if (this->MoveToLocation(this->ResetLocation) == EPathFollowingRequestResult::AlreadyAtGoal)
						{
							ControlledCharacter->SetActorRotation(this->ResetRotation);
						}

						MovementSpeed = 1.5f;
					}
					else if (ShouldAttack)
					{
						this->StopMovement();

						ControlledCharacter->StartAttack();

						this->AttackCooldownTimeRemaining = Params.AttackCooldown;
					}
					else if (ShouldWander && Params.CanWander)
					{
						if (!WasWandering)
						{
							this->FindWanderLocation(ControlledCharacter->GetActorLocation());
						}

						if (this->MoveToLocation(this->WanderLocation) == EPathFollowingRequestResult::RequestSuccessful)
						{
							this->IsWandering = true;

							this->WanderLingerTimeRemaining = FMath::RandRange(0.5f, 1.5f);
						}
						else
						{
							if (this->WanderLingerTimeRemaining > 0.0f)
							{
								this->IsWandering = true;
							}
							else if (FMath::SRand() <= 0.2f)
							{
								this->IsWandering = true;

								this->FindWanderLocation(ControlledCharacter->GetActorLocation());
							}
						}

						MovementSpeed = 0.5f;
					}
					else if (ShouldApproach && Params.CanApproach)
					{
						this->MoveToLocation(PlayerCharacter->GetActorLocation(), -1.0f, true, true, false, true, 0, false);

						MovementSpeed = 1.0f;
					}
					else
					{
						this->StopMovement();
					}
				}

				// Step
				if (this->ReverseStepProgress > 0.0f)
				{
					this->ReverseStepProgress -= DeltaSeconds / Params.StepSpeed;
				}

				if (this->ReverseStepProgress <= 0.0f)
				{
					if (this->GetMoveStatus() == EPathFollowingStatus::Moving)
					{
						this->ReverseStepProgress += 1.0f;
					}
					else
					{
						this->ReverseStepProgress = 0.0f;
					}
				}

				float const StepPower = 5.0f;
				float const StepWeight = FMath::Pow(1.0f - (FMath::Abs((1.0f - ReverseStepProgress) - 0.5f) * 2.0f), StepPower);
				float const StepCorrection = FMath::Pow(0.67f, StepPower);

				ControlledCharacter->GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, 30.0f * StepWeight));
				ControlledCharacter->GetCharacterMovement()->MaxWalkSpeed = Params.DefaultMovementSpeed / StepCorrection * StepWeight * MovementSpeed;
				ControlledCharacter->GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f / StepCorrection * StepWeight, 0.0f);
			}
		}
	}
}

void AEnemyAIController::FindWanderLocation(FVector OriginLocation)
{
	this->WanderLocation = OriginLocation + FVector(FMath::RandPointInCircle(300.0f), OriginLocation.Z);
}
