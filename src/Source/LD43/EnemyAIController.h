// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "EnemyAIController.generated.h"

UCLASS()
class LD43_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AEnemyAIController();

public:
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector ResetLocation;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator ResetRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool IsWandering;

private:
	UPROPERTY()
	float AggroLingerTimeRemaining;

	UPROPERTY()
	float AttackCooldownTimeRemaining;

	UPROPERTY()
	float WanderLingerTimeRemaining;

	UPROPERTY()
	FVector WanderLocation;

	UPROPERTY()
	float ReverseStepProgress;

	UPROPERTY()
	float SpawnTimeRemaining;

	void FindWanderLocation(FVector OriginLocation);
};
