// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainPlayerController.h"

#include "ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "MainCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"

AMainPlayerController::AMainPlayerController()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> HUDWidgetClass(TEXT("/Game/UI/BP_HUDWidget.BP_HUDWidget_C"));
	if (HUDWidgetClass.Succeeded())
	{
		this->HUDWidgetClass = HUDWidgetClass.Class;
	}
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	this->HUDWidget = CreateWidget(this, this->HUDWidgetClass);
	this->HUDWidget->AddToViewport();

	this->SetInputMode(FInputModeGameOnly());
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	this->InputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AMainPlayerController::StartJumping);
	this->InputComponent->BindAction(TEXT("Jump"), IE_Released, this, &AMainPlayerController::StopJumping);
	this->InputComponent->BindAction(TEXT("Attack"), IE_Pressed, this, &AMainPlayerController::Attack);
	this->InputComponent->BindAction(TEXT("Menu"), IE_Pressed, this, &AMainPlayerController::Menu);

	this->InputComponent->BindAxis(TEXT("MoveForward"), this, &AMainPlayerController::MoveForward);
	this->InputComponent->BindAxis(TEXT("MoveRight"), this, &AMainPlayerController::MoveRight);
	this->InputComponent->BindAxis(TEXT("Turn"), this, &APlayerController::AddYawInput);
	this->InputComponent->BindAxis(TEXT("Look"), this, &AMainPlayerController::Look);
}

void AMainPlayerController::StartJumping()
{
	ACharacter* const PlayerCharacter = Cast<ACharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->Jump();
	}
}

void AMainPlayerController::StopJumping()
{
	ACharacter* const PlayerCharacter = Cast<ACharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->StopJumping();
	}
}

void AMainPlayerController::Attack()
{
	AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->Attack();
	}
}

void AMainPlayerController::Menu()
{
	UGameplayStatics::OpenLevel(this, TEXT("Menu"));
}

void AMainPlayerController::MoveForward(float Value)
{
	ACharacter* const PlayerCharacter = Cast<ACharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->AddMovementInput(PlayerCharacter->GetActorForwardVector(), Value);
	}
}

void AMainPlayerController::MoveRight(float Value)
{
	ACharacter* const PlayerCharacter = Cast<ACharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->AddMovementInput(PlayerCharacter->GetActorRightVector(), Value);
	}
}

void AMainPlayerController::Look(float Value)
{
	AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(this->GetPawn());
	if (PlayerCharacter != nullptr)
	{
		PlayerCharacter->GetCameraBoom()->RelativeRotation = FRotator(FMath::Clamp(PlayerCharacter->GetCameraBoom()->RelativeRotation.Pitch + Value, -60.0f, 40.0f), 0.0f, 0.0f);
	}
}
