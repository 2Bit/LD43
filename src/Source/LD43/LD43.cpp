// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "LD43.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, LD43, "LD43");
