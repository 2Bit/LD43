// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "EnemySpawner.h"

#include "EnemyCharacter.h"
#include "Engine/World.h"

AEnemySpawner::AEnemySpawner()
{
	this->Root = this->CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	this->RootComponent = this->Root;

	this->bBeginSpawned = true;
	this->bEnsureSpawned = true;

	this->PrimaryActorTick.bCanEverTick = true;
}

void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

	if (this->bBeginSpawned)
	{
		this->Spawn();
	}
}

void AEnemySpawner::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (this->IsSpawning())
	{
		this->SpawnTimeRemaining -= DeltaSeconds;

		if (!this->IsSpawning())
		{
			this->Spawn();
		}
	}
	else if (this->bEnsureSpawned && (this->SpawnedEnemy == nullptr || this->SpawnedEnemy->IsActorBeingDestroyed()))
	{
		this->BeginSpawn();
	}
}

bool AEnemySpawner::IsSpawning() const
{
	return this->SpawnTimeRemaining > 0.0f;
}

void AEnemySpawner::BeginSpawn()
{
	this->SpawnTimeRemaining = 3.0f;
}

void AEnemySpawner::Spawn()
{
	if (this->EnemyClass != nullptr)
	{
		this->SpawnedEnemy = this->GetWorld()->SpawnActor<AEnemyCharacter>(*this->EnemyClass, this->GetActorLocation(), this->GetActorRotation());
		this->SpawnedEnemy->SpawnDefaultController();
	}
}
