// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "EnemyCharacter.h"

#include "ConstructorHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "VoxelAnimationComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnemyAIController.h"
#include "MainCharacter.h"
#include "VoxelComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

AEnemyCharacter::AEnemyCharacter()
{
	this->Animation = this->CreateDefaultSubobject<UVoxelAnimationComponent>(TEXT("Animation"));
	this->Animation->SetupAttachment(this->GetMesh());
	this->Animation->RelativeLocation = FVector(0.0f, 0.0f, -88.0f);
	this->Animation->RelativeRotation = FRotator(0.0f, 180.0f, 0.0f);
	this->Animation->RelativeScale3D = FVector(0.3f, 0.3f, 0.3f);

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialBase(TEXT("Material'/Game/Materials/M_Character.M_Character'"));
	if (MaterialBase.Succeeded())
	{
		this->MaterialBase = MaterialBase.Object;
	}

	this->GetCharacterMovement()->MaxWalkSpeed = 300.0f;
	this->GetCharacterMovement()->bOrientRotationToMovement = true;

	this->bUseControllerRotationYaw = false;

	this->AIControllerClass = AEnemyAIController::StaticClass();

	this->HitPoints = 1;
	this->AttackDelayTime = 1.0f;

	this->PrimaryActorTick.bCanEverTick = true;
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->Material = UMaterialInstanceDynamic::Create(this->MaterialBase, this);
	this->Animation->SetMaterial(0, this->Material);
}

void AEnemyCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (this->IsAttacking())
	{
		this->AttackDelayTimeRemaining -= DeltaSeconds;

		if (!this->IsAttacking())
		{
			this->OnPerformAttack();

			this->StopAttack();
		}
	}

	if (this->Material != nullptr && this->MaterialFlashRemaining > 0.0f)
	{
		this->MaterialFlashRemaining -= DeltaSeconds * 8.0f;

		if (this->MaterialFlashRemaining < 0.0f)
		{
			this->MaterialFlashRemaining = 0.0f;
		}

		this->Material->SetScalarParameterValue(TEXT("Flash"), this->MaterialFlashRemaining);
	}

	if (!this->Controller->IsActorTickEnabled() && !this->GetAnimation()->IsDestroying())
	{
		this->Destroy();
	}
}

float AEnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	this->HitPoints -= DamageAmount;

	this->MaterialFlashRemaining = 1.0f;
	
	if (DamageCauser != nullptr)
	{
		this->GetCharacterMovement()->AddImpulse((DamageCauser->GetActorForwardVector() + FVector(0.0f, 0.0f, 0.2f)) * 1000.0f, true);

		AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(DamageCauser);
		if (PlayerCharacter != nullptr)
		{
			this->GetAnimation()->DestroyChunk(PlayerCharacter->GetFistLocation(), DamageCauser->GetActorForwardVector(), 12);
		}
	}

	if (this->HitPoints <= 0)
	{
		AMainCharacter* const PlayerCharacter = Cast<AMainCharacter>(DamageCauser);
		if (PlayerCharacter != nullptr)
		{
			PlayerCharacter->Score += 100 * PlayerCharacter->GetFavour(this->Favour);

			PlayerCharacter->ReduceFavour(this->Favour);
			PlayerCharacter->ReduceFavour(this->Favour);

			if (this->Favour != EFavour::Hermes)
			{
				PlayerCharacter->IncreaseFavour(EFavour::Hermes);
			}

			if (this->Favour != EFavour::Ares)
			{
				PlayerCharacter->IncreaseFavour(EFavour::Ares);
			}

			if (this->Favour != EFavour::Apollo)
			{
				PlayerCharacter->IncreaseFavour(EFavour::Apollo);
			}
		}

		this->Kill();
	}
	else
	{
		if (this->HitSounds.Num() > 0)
		{
			int32 const HitSoundsIndex = FMath::RandRange(0, this->HitSounds.Num() - 1);
			UGameplayStatics::PlaySoundAtLocation(this, this->HitSounds[HitSoundsIndex], this->GetActorLocation());
		}
	}

	this->StopAttack();

	return DamageAmount;
}

void AEnemyCharacter::Kill()
{
	if (this->Controller->IsActorTickEnabled())
	{
		this->Controller->SetActorTickEnabled(false);

		this->GetAnimation()->DestroyAll();

		this->GetCharacterMovement()->GravityScale = 0.0f;
		this->GetCharacterMovement()->StopMovementImmediately();
		//this->GetCharacterMovement()->ClearAccumulatedForces();
		this->GetCharacterMovement()->Velocity = FVector(0.0f, 0.0f, 0.0f);

		this->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		if (this->DeathSounds.Num() > 0)
		{
			int32 const DeathSoundsIndex = FMath::RandRange(0, this->DeathSounds.Num() - 1);
			UGameplayStatics::PlaySoundAtLocation(this, this->DeathSounds[DeathSoundsIndex], this->GetActorLocation());
		}
	}
}

bool AEnemyCharacter::IsAttacking() const
{
	return this->AttackDelayTimeRemaining > 0.0f;
}

float AEnemyCharacter::GetAttackProgress() const
{
	if (this->IsAttacking())
	{
		return 1.0f - FMath::Max(0.0f, this->AttackDelayTimeRemaining / this->AttackDelayTime);
	}

	return 0.0f;
}

void AEnemyCharacter::StartAttack()
{
	this->StopAttack();

	this->AttackDelayTimeRemaining = this->AttackDelayTime;

	this->OnStartAttack();
}

void AEnemyCharacter::StopAttack()
{
	this->AttackDelayTimeRemaining = 0.0f;

	this->OnStopAttack();
}

void AEnemyCharacter::OnStartAttack_Implementation()
{
	if (this->StartAttackSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, this->StartAttackSound, this->GetActorLocation());
	}
}

void AEnemyCharacter::OnPerformAttack_Implementation()
{
}

void AEnemyCharacter::OnStopAttack_Implementation()
{
}
