// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "VoxelAnimationComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class LD43_API UVoxelAnimationComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UVoxelAnimationComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetMaterial(int32 ElementIndex, class UMaterialInterface* Material);

	UFUNCTION(BlueprintCallable)
	void DestroyRandomChunk(int32 Size);

	UFUNCTION(BlueprintCallable)
	void DestroyChunk(FVector const& RayOrigin, FVector const& RayDirection, int32 Size);

	UFUNCTION(BlueprintCallable)
	void DestroyVoxel(class UVoxelComponent* Frame, FIntVector const& VoxelIndex, bool PlayEffect);

	UFUNCTION(BlueprintCallable)
	void DestroyAll();

	UFUNCTION(BlueprintCallable)
	bool IsDestroying() const;

	UFUNCTION(BlueprintCallable)
	bool AddFrame(FName Name, class UVoxel* Frame);

	UFUNCTION(BlueprintCallable)
	bool ShowFrame(FName Name, float Delay = 0.0f);

    UFUNCTION(BlueprintCallable)
    class UVoxelComponent* GetCurrentFrame();

private:
	UPROPERTY()
	class UParticleSystem* ChunkParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	TMap<FName, class UVoxel*> FrameFeed;

	UPROPERTY()
	TMap<FName, class UVoxelComponent*> Frames;

	UPROPERTY()
	class UVoxelComponent* CurrentFrame;

	UPROPERTY()
	FName DelayedFrameName;

	UPROPERTY()
	float DelayedFrameTime;

	UPROPERTY()
	uint32 bIsRunning : 1;

	UPROPERTY()
	int32 DestroyIndex;

	UPROPERTY()
	TArray<FIntVector> Keys;

	void FeedFrames();
};
