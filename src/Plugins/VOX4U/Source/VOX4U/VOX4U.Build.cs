// Copyright 2016-2018 mik14a / Admix Network. All Rights Reserved.

using UnrealBuildTool;

public class VOX4U : ModuleRules
{
	public VOX4U(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(
			new string[] {
                //"Runtime/Engine/Classes/Components"
			}
		);

		PrivateIncludePaths.AddRange(
			new string[] {
                //"Runtime/Engine/Classes/Components"
            }
		);

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
                "Engine"
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
			}
		);

		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
			}
		);
	}
}
