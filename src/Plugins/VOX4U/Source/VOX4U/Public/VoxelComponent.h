// Copyright 2016-2018 mik14a / Admix Network. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "VoxelComponent.generated.h"

class UInstancedStaticMeshComponent;
class UStaticMesh;
class UVoxel;
class UCurveFloat;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FVoxelEffectDelegate,
UVoxelComponent*, VoxelComponent,
FIntVector, VoxelIdx,
FTransform, VoxelTransform,
FTransform&, OutTransform);

/**
 * Voxel component
 */
UCLASS(BlueprintType)
class VOX4U_API UVoxelComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = VoxelComponent)
	FBoxSphereBounds CellBounds;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VoxelComponent)
	bool bHideUnbeheld;


protected:
	UPROPERTY(EditAnywhere, EditFixedSize, BlueprintReadWrite, Category = VoxelComponent)
	TArray<UStaticMesh*> Mesh;

	UPROPERTY(EditAnywhere, EditFixedSize, BlueprintReadWrite, Category = VoxelComponent)
	TMap<FIntVector, uint8> Cell;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = VoxelComponent)
	UVoxel* Voxel;

	UPROPERTY()
	TArray<UInstancedStaticMeshComponent*> InstancedStaticMeshComponents;

public:
	UPROPERTY()
	TMap<FIntVector, int32> VoxelIdToInstanceId;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UCurveFloat* UsefulCurve;

    UPROPERTY()
    TMap<FIntVector, int32> HiddenVoxelIds;

    float TimeActive;

public:
    UPROPERTY()
    FVoxelEffectDelegate VoxelEffectDelegate;

	UVoxelComponent();

#if WITH_EDITOR

	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif // WITH_EDITOR

	void SetVoxel(class UVoxel* InVoxel, bool bForce = false);

	const UVoxel* GetVoxel() const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	void AddVoxel();

	UFUNCTION(BlueprintCallable, Category = Voxel)
	void ClearVoxel();

	UFUNCTION(BlueprintCallable, Category = Voxel)
	bool IsUnbeheldVolume(const FIntVector& InVector) const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	void GetVoxelTransform(const FIntVector& InVector, FTransform& OutVoxelTransform, bool bWorldSpace = false) const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	FColor GetVoxelVertexColor(const FIntVector& InVector) const;

	virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;

	const TArray<UInstancedStaticMeshComponent*>& GetInstancedStaticMeshComponent() const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	bool FindIntersectingVoxel(FVector WorldRayLocation, FVector WorldRayDirection, FIntVector& VoxelIndex3d, int SearchVoxelRadius) const;

	virtual void SetMaterial(int32 ElementIndex, class UMaterialInterface* Material) override;
	
	UFUNCTION(BlueprintCallable, Category = Voxel)
	bool HideVoxel(FIntVector Voxel3dIdx);

	UFUNCTION(BlueprintCallable, Category = Voxel)
	bool IsVisibleVoxel(FIntVector Voxel3dIdx) const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	FIntVector GetRandomVisibleVoxel() const;

	FBox CalculateCSBounds() const;
    
    void ShowVoxels();

	FIntVector ConvertCSLocationToVoxelIndex(FVector LocationCS) const;

	UFUNCTION(BlueprintCallable, Category = Voxel)
	TArray<FIntVector> GetSurroundingVoxels(FIntVector Origin, int32 SearchRadius) const;
	
	virtual void OnComponentCreated() override;

    void UpdateVoxelEffects(float DeltaTime);

    //void UpdateColorData();

    UFUNCTION()
    void ApplySpawnEffect(UVoxelComponent* VoxelComponent, FIntVector VoxelIdx, FTransform VoxelTransform, FTransform& OutTransform);

private:

	void InitVoxel();


};
