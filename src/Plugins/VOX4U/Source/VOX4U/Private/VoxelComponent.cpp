// Copyright 2016-2018 mik14a / Admix Network. All Rights Reserved.

#include "VoxelComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Containers/ArrayBuilder.h"
#include "Engine/StaticMesh.h"
#include "Voxel.h"
#include "Curves/CurveFloat.h"
#include "ConstructorHelpers.h"

UVoxelComponent::UVoxelComponent()
	: CellBounds(FVector::ZeroVector, FVector(100.f, 100.f, 100.f), 100.f)
	, bHideUnbeheld(true)
	, Mesh()
	, Cell()
    , TimeActive(-1.0f)
	//, Voxel(nullptr)
	, InstancedStaticMeshComponents()
{
    static ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TEXT("CurveFloat'/Game/SpawnCurve.SpawnCurve'"));
    if (Curve.Succeeded())
    {
        this->UsefulCurve = Curve.Object;
    }
    
}

#if WITH_EDITOR
void UVoxelComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	static const FName NAME_HideUnbeheld = FName(TEXT("bHideUnbeheld"));
	static const FName NAME_Mesh = FName(TEXT("Mesh"));
	static const FName NAME_Voxel = FName(TEXT("Voxel"));
	if (PropertyChangedEvent.Property) {
		if (PropertyChangedEvent.Property->GetFName() == NAME_HideUnbeheld) {
			ClearVoxel();
			AddVoxel();
		} else if (PropertyChangedEvent.Property->GetFName() == NAME_Mesh) {
			FBoxSphereBounds WorkingBounds(ForceInit);
			for (int32 i = 0; i < Mesh.Num(); ++i) {
				InstancedStaticMeshComponents[i]->SetStaticMesh(Mesh[i]);
				if (Mesh[i]) {
					WorkingBounds = WorkingBounds + Mesh[i]->GetBounds();
				}
			}
			CellBounds = WorkingBounds;
		} else if (PropertyChangedEvent.Property->GetFName() == NAME_Voxel) {
			SetVoxel(Voxel, true);
		}
	}
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif // WITH_EDITOR

void UVoxelComponent::SetVoxel(class UVoxel* InVoxel, bool bForce /*= false*/)
{
	//if (Voxel != InVoxel || bForce) {
		Voxel = InVoxel;
	//	InitVoxel();
	//}
}

const UVoxel* UVoxelComponent::GetVoxel() const
{
	return Voxel;
}

//void UVoxelComponent::UpdateColorData()
//{
//#if WITH_EDITOR
//
//    for (int i = 0; i < this->InstancedStaticMeshComponents.Num(); i++)
//    {
//        TMap<FVector, FColor> OutData;
//        this->InstancedStaticMeshComponents[i]->GetStaticMesh()->GetVertexColorData(OutData);
//        this->InstancedMeshColors.Add(i, OutData.CreateIterator().Value());
//    }
//
//#endif
//}

void UVoxelComponent::InitVoxel()
{
	CellBounds = FBoxSphereBounds(FVector::ZeroVector, FVector(100.f, 100.f, 100.f), 100.f);
	Mesh.Empty();
	Cell.Empty();
	InstancedStaticMeshComponents.Empty();
	this->VoxelIdToInstanceId.Empty();
	if (Voxel) {
		CellBounds = Voxel->CellBounds;
		Mesh = Voxel->Mesh;
		Cell = Voxel->Voxel;
		for (int32 i = 0; i < Mesh.Num(); ++i) {
			FName UniqueObjectName = MakeUniqueObjectName(this, UInstancedStaticMeshComponent::StaticClass(), FName("VoxelInstancedStaticMesh"));
			UInstancedStaticMeshComponent* Proxy = NewObject<UInstancedStaticMeshComponent>(this, UniqueObjectName, RF_Transactional);
			Proxy->SetStaticMesh(Mesh[i]);
			Proxy->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			Proxy->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform, NAME_None);
			Proxy->RegisterComponent();
			InstancedStaticMeshComponents.Add(Proxy);
		}
		AddVoxel();
        //UpdateColorData();
	}
}

void UVoxelComponent::AddVoxel()
{
	FVector Offset = Voxel->bXYCenter ? FVector((float)Voxel->Size.X, (float)Voxel->Size.Y, 0.f) * CellBounds.BoxExtent : FVector::ZeroVector;
	for (auto& VoxelIdxKeyVal : Voxel->Voxel) {
		FIntVector VoxelId = VoxelIdxKeyVal.Key;
		if (bHideUnbeheld && IsUnbeheldVolume(VoxelId)) continue;
		FVector Translation = FVector(VoxelId) * CellBounds.BoxExtent * 2 -CellBounds.Origin + CellBounds.BoxExtent - Offset;
		FTransform Transform(FQuat::Identity, Translation, FVector(1.f));
		UInstancedStaticMeshComponent* Proxy = InstancedStaticMeshComponents[VoxelIdxKeyVal.Value];
		int32 InstanceIdx = Proxy->AddInstance(Transform);
		this->VoxelIdToInstanceId.Add(VoxelId, InstanceIdx);
	}
}

void UVoxelComponent::ClearVoxel()
{
	for (int32 i = 0; i < Mesh.Num(); ++i) {
		InstancedStaticMeshComponents[i]->ClearInstances();
	}
	this->VoxelIdToInstanceId.Empty();
}

bool UVoxelComponent::IsUnbeheldVolume(const FIntVector& InVector) const
{
	static const TArray<FIntVector> Direction = TArrayBuilder<FIntVector>()
		.Add(FIntVector(+0, +0, +1))	// Up
		.Add(FIntVector(+0, +0, -1))	// Down
		.Add(FIntVector(+1, +0, +0))	// Forward
		.Add(FIntVector(-1, +0, +0))	// Backward
		.Add(FIntVector(+0, +1, +0))	// Right
		.Add(FIntVector(+0, -1, +0));	// Left
	int count = 0;
	for (int i = 0; i < Direction.Num(); ++i) {
		if (Voxel->Voxel.Contains(InVector + Direction[i])) {
			++count;
		}
	}
	return Direction.Num() == count;
}

void UVoxelComponent::GetVoxelTransform(const FIntVector& InVector, FTransform& OutVoxelTransform, bool bWorldSpace /*= false*/) const
{
	//if (!Cell.Contains(InVector)) return false;
	FVector Offset = Voxel->bXYCenter ? FVector((float)Voxel->Size.X, (float)Voxel->Size.Y, 0.f) * CellBounds.BoxExtent : FVector::ZeroVector;
	FVector Translation = FVector(InVector) * CellBounds.BoxExtent * 2 - CellBounds.Origin + CellBounds.BoxExtent - Offset;
	OutVoxelTransform = FTransform(FQuat::Identity, Translation, FVector(1.f));
	if (bWorldSpace) {
		OutVoxelTransform = OutVoxelTransform * GetComponentToWorld();
	}
	//return true;
}

FColor UVoxelComponent::GetVoxelVertexColor(const FIntVector& InVector) const
{
	if (this->Cell.Contains(InVector))
	{
		uint8 const InstancedStaticMeshIndex = this->Cell[InVector];

		//TMap<FVector, FColor> OutData;
		//this->InstancedStaticMeshComponents[InstancedStaticMeshIndex]->GetStaticMesh()->GetVertexColorData(OutData);

        //if (this->InstancedMeshColors.Contains(InstancedStaticMeshIndex)) return this->InstancedMeshColors[InstancedStaticMeshIndex];
        if(this->Voxel->MeshColors.Num() > InstancedStaticMeshIndex) return this->Voxel->MeshColors[InstancedStaticMeshIndex];
		//return OutData.CreateIterator().Value();
	}

	return FColor();
}

FBoxSphereBounds UVoxelComponent::CalcBounds(const FTransform& LocalToWorld) const
{
	if (this->Voxel == nullptr)
	{
		return FBoxSphereBounds( FBox(FVector(-100,-100,100), FVector(100,100,100)) );
	}

	FBox BoundsCS = this->CalculateCSBounds();
	FBoxSphereBounds BoxSphereBounds = FBoxSphereBounds(BoundsCS);
	return BoxSphereBounds.TransformBy(LocalToWorld);
}

const TArray<UInstancedStaticMeshComponent*>& UVoxelComponent::GetInstancedStaticMeshComponent() const
{
	return InstancedStaticMeshComponents;
}

bool UVoxelComponent::FindIntersectingVoxel(FVector RayLocationWS, FVector RayDirectionWS, FIntVector& VoxelIndex3d, int SearchVoxelRadius) const
{

	FBox BBCS = this->CalculateCSBounds();
	FBox BBWS = BBCS.TransformBy(this->GetComponentToWorld());
	FVector RayNormalWS = RayDirectionWS.GetSafeNormal();


	//test if ray will even hit bounds of voxel component
	FVector BoxOriginWS = GetComponentToWorld().GetLocation();
	FVector OriginProjected = (BoxOriginWS - RayLocationWS).ProjectOnToNormal(RayNormalWS);
	FVector ProjectedWorld = OriginProjected + RayLocationWS;

	bool bIsInBox = BBWS.IsInsideOrOn(ProjectedWorld);

	if (!bIsInBox)
	{
		return false;

	}

	if (bIsInBox)
	{
		//proceed to test for the voxel that is hit (if any)
		FTransform ToCS = this->GetComponentToWorld().Inverse();
		FVector RayLocationCS = ToCS.TransformPosition(RayLocationWS);
		FVector RayNormalCS = ToCS.TransformVector(RayDirectionWS);
		FVector BoxSurfacePtCS = BBCS.GetClosestPointTo(RayLocationCS);
		float DistSq = RayLocationCS.SizeSquared();
		FVector RayEndCS = RayLocationCS + (RayNormalCS * DistSq);
		FVector HitLocationCS;
		FVector HitNormalCS;
		float HitTime;
		FMath::LineExtentBoxIntersection(BBCS, RayLocationCS, RayEndCS, FVector::ZeroVector, HitLocationCS, HitNormalCS, HitTime);

		//convert 3d component-space position into voxel cell coordinate
		//FTransform OutTransform;
		//this->GetVoxelTransform(FIntVector(1, 1, 1), OutTransform, false);

		bool bHitVoxel = false;

		while (!bHitVoxel)
		{
			FIntVector HitVoxelIdx = this->ConvertCSLocationToVoxelIndex(HitLocationCS);

			if (this->IsVisibleVoxel(HitVoxelIdx))
			{
				VoxelIndex3d = HitVoxelIdx;
				return true;
			}
			//did not hit, so searcher deeper into the voxels
			else
			{
				//try searching neighbours
				TArray<FIntVector> Neighbours = this->GetSurroundingVoxels(HitVoxelIdx, SearchVoxelRadius);
				for (FIntVector& NeighbourVoxel : Neighbours)
				{
					if (this->IsVisibleVoxel(NeighbourVoxel))
					{
						VoxelIndex3d = NeighbourVoxel;
						return true;
					}
				}

				HitLocationCS += (RayNormalCS * this->CellBounds.BoxExtent.X * 2);
				//check if we have passed through the voxels entirely
				if (!BBCS.IsInsideOrOn(HitLocationCS))
				{
					break;
				}
			}
		}

		

		return false;
	}

	return false;
}

void UVoxelComponent::SetMaterial(int32 ElementIndex, class UMaterialInterface* Material)
{
	Super::SetMaterial(ElementIndex, Material);

	for (int i = 0; i < this->InstancedStaticMeshComponents.Num(); ++i)
	{
		this->InstancedStaticMeshComponents[i]->SetMaterial(ElementIndex, Material);
	}
}

void UVoxelComponent::ShowVoxels()
{
    TArray<FIntVector> VoxelToInstanceKeys;
    HiddenVoxelIds.GetKeys(VoxelToInstanceKeys);
    for (int i = 0; i < VoxelToInstanceKeys.Num(); i++)
    {
        auto VoxelToInstanceKey = VoxelToInstanceKeys[i];
        auto VoxelToInstanceValue = HiddenVoxelIds[VoxelToInstanceKey];

        uint8 InstanceStaticMeshIdx = this->Voxel->Voxel[VoxelToInstanceKey];
        UInstancedStaticMeshComponent* InstancedStaticMeshComp = this->InstancedStaticMeshComponents[InstanceStaticMeshIdx];

        FTransform VoxelTransform;
        this->GetVoxelTransform(VoxelToInstanceKey, VoxelTransform, true);

        int32 InstanceIdx = VoxelToInstanceValue;
        InstancedStaticMeshComp->UpdateInstanceTransform(InstanceIdx, VoxelTransform, true, true, true);
        this->HiddenVoxelIds.Remove(VoxelToInstanceKey);
        this->VoxelIdToInstanceId.Add(VoxelToInstanceKey, InstanceIdx);
    }
}

bool UVoxelComponent::HideVoxel(FIntVector Voxel3dIdx)
{
	if (this->Voxel->Voxel.Contains(Voxel3dIdx))
	{
		uint8 InstanceStaticMeshIdx = this->Voxel->Voxel[Voxel3dIdx];
		UInstancedStaticMeshComponent* InstancedStaticMeshComp = this->InstancedStaticMeshComponents[InstanceStaticMeshIdx];
		if (this->VoxelIdToInstanceId.Contains(Voxel3dIdx))
		{
			int32 InstanceIdx = this->VoxelIdToInstanceId[Voxel3dIdx];
			InstancedStaticMeshComp->UpdateInstanceTransform(InstanceIdx, FTransform(FRotator::ZeroRotator, FVector::ZeroVector, FVector::ZeroVector), true, true, true);
			//InstancedStaticMeshComp->InstanceUpdateCmdBuffer.HideInstance(InstanceIdx);
			//remove from id to instance id map to indicate it is gone now
			this->VoxelIdToInstanceId.Remove(Voxel3dIdx);
            this->HiddenVoxelIds.Add(Voxel3dIdx, InstanceIdx);
			return true;
		}
	}
	return false;
}

bool UVoxelComponent::IsVisibleVoxel(FIntVector Voxel3dIdx) const
{
	bool IsInVoxelMesh = this->Cell.Contains(Voxel3dIdx);
	if (IsInVoxelMesh)
	{
		bool IsInVoxelInstances = this->VoxelIdToInstanceId.Contains(Voxel3dIdx);
		return IsInVoxelInstances;
	}
	return false;
}

FIntVector UVoxelComponent::GetRandomVisibleVoxel() const
{
	TArray<FIntVector> OutKeys;
	this->VoxelIdToInstanceId.GenerateKeyArray(OutKeys);
	if (OutKeys.Num() > 0)
	{
		int32 RandIdx = FMath::RandHelper(OutKeys.Num() - 1);
		return OutKeys[RandIdx];
	}

	return FIntVector();
}

FBox UVoxelComponent::CalculateCSBounds() const
{
	FTransform MinCS;
	this->GetVoxelTransform(FIntVector(0, 0, 0), MinCS, false);

	FTransform MaxCS;
	this->GetVoxelTransform(this->Voxel->Size, MaxCS, false);

	//FVector CubeSize = CellBounds.BoxExtent;
	//FIntVector VoxelsDims = this->Voxel->Size;
	//FVector BBSize = FVector(CubeSize.X * VoxelsDims.X, CubeSize.Y * VoxelsDims.Y, CubeSize.Z * VoxelsDims.Z);

	////origin is at base
	//FVector Min = FVector(-BBSize.X * 0.5f, -BBSize.Y * 0.5f, 0);
	//FVector Max = FVector(BBSize.X * 0.5f, BBSize.Y * 0.5f, BBSize.Z);
	return FBox(MinCS.GetTranslation(), MaxCS.GetTranslation());
}

FIntVector UVoxelComponent::ConvertCSLocationToVoxelIndex(FVector LocationCS) const
{
	//my assumption is original author has put 0,0,0 at minimum bounds

	FVector Offset = Voxel->bXYCenter ? FVector((float)Voxel->Size.X, (float)Voxel->Size.Y, 0.f) * CellBounds.BoxExtent : FVector::ZeroVector;
	//FVector Translation = FVector(InVector) * CellBounds.BoxExtent * 2 - CellBounds.Origin + CellBounds.BoxExtent - Offset;
	// x = Translation
	// y = FVector(InVector) 
	// a = CellBounds.BoxExtent
	// b = CellBounds.Origin
	// c = Offset
	// x = y * a * 2 - b + a - c
	// x = 2ay - b + a - c
	// x + c - a + b = 2ay
	// y = (x + c - a + b) / 2a


	FVector a = CellBounds.BoxExtent;
	FVector b = CellBounds.Origin;
	FVector c = Offset;
	FVector x = LocationCS;
	FVector FVoxelIdx3d = (x + c - a + b) / (2.0f * a);
	FIntVector VoxelIdx3d = FIntVector(FVoxelIdx3d);

	FVector Translation = FVector(VoxelIdx3d) * CellBounds.BoxExtent * 2 - CellBounds.Origin + CellBounds.BoxExtent - Offset;

	/*FIntVector VoxelsDims = this->Voxel->Size;
	FVector CubeSize = CellBounds.BoxExtent;

	int32 XIdx = FMath::FloorToInt(LocationCS.X / CubeSize.X + VoxelsDims.X * 0.5f);
	int32 YIdx = FMath::FloorToInt(LocationCS.Y / CubeSize.Y + VoxelsDims.Y * 0.5f);
	int32 ZIdx = FMath::FloorToInt(LocationCS.Z / CubeSize.Z);*/
	
	return VoxelIdx3d;
}

TArray<FIntVector> UVoxelComponent::GetSurroundingVoxels(FIntVector Origin, int32 SearchRadius) const
{
	TArray<FIntVector> NeighbourVoxels;
	int32 MinX = Origin.X - SearchRadius;
	int32 MinY = Origin.Y - SearchRadius;
	int32 MinZ = Origin.Z - SearchRadius;
	int32 MaxX = Origin.X + SearchRadius;
	int32 MaxY = Origin.Y + SearchRadius;
	int32 MaxZ = Origin.Z + SearchRadius;

	for (int32 CurX = MinX; CurX <= MaxX; CurX++)
	{
		for (int32 CurY = MinY; CurY <= MaxY; CurY++)
		{
			for (int32 CurZ = MinZ; CurZ <= MaxZ; CurZ++)
			{
				if (FMath::Abs(CurX - Origin.X) + FMath::Abs(CurY - Origin.Y) + FMath::Abs(CurZ - Origin.Z) <= SearchRadius)
				{
					NeighbourVoxels.Add(FIntVector(CurX, CurY, CurZ));
				}
			}
		}
	}
	return NeighbourVoxels;
}

void UVoxelComponent::UpdateVoxelEffects(float DeltaTime)
{
    TimeActive = TimeActive < 0.0f ? 0.0f : TimeActive + DeltaTime;

    TArray<FIntVector> VoxelToInstanceKeys; 
    VoxelIdToInstanceId.GetKeys(VoxelToInstanceKeys);
    for (int i = 0; i < VoxelToInstanceKeys.Num(); i++)
    {
        auto VoxelToInstanceKey = VoxelToInstanceKeys[i];
        auto VoxelToInstanceValue = VoxelIdToInstanceId[VoxelToInstanceKey];
        if (this->IsUnbeheldVolume(VoxelToInstanceKey)) {
            this->HideVoxel(VoxelToInstanceKey);
            continue;
        }

        FTransform VoxelTransform;
        this->GetVoxelTransform(VoxelToInstanceKey, VoxelTransform);
        FTransform OutVoxelTransform;
        this->VoxelEffectDelegate.Broadcast(this, VoxelToInstanceKey, VoxelTransform, OutVoxelTransform);

        if (this->Voxel->Voxel.Contains(VoxelToInstanceKey))
        {
            uint8 InstanceStaticMeshIdx = this->Voxel->Voxel[VoxelToInstanceKey];
            UInstancedStaticMeshComponent* InstancedStaticMeshComp = this->InstancedStaticMeshComponents[InstanceStaticMeshIdx];
            if (this->VoxelIdToInstanceId.Contains(VoxelToInstanceKey))
            {
                int32 InstanceIdx = this->VoxelIdToInstanceId[VoxelToInstanceKey];
                InstancedStaticMeshComp->UpdateInstanceTransform(InstanceIdx, OutVoxelTransform, false, true, true);
            }
        }
    }
}

void UVoxelComponent::ApplySpawnEffect(UVoxelComponent * VoxelComponent, FIntVector VoxelIdx, FTransform VoxelTransform, FTransform & OutTransform)
{
    const float StartHeight = 1200.0f;
    const float VerticalOffset = 500.0f;
    const float TotalTime = 1.6f;
    const float HorizontalOffset = 35.0f;
    float CurrentTime = FMath::Clamp(VoxelComponent->TimeActive, 0.0f, TotalTime);
    float Alpha = 1.0f - (CurrentTime / TotalTime);
    float OriginalAlpha = Alpha;

    if (this->UsefulCurve)
    {
        Alpha = this->UsefulCurve->GetFloatValue(Alpha);
    }

    OutTransform = VoxelTransform;
    FVector Translation = VoxelTransform.GetTranslation();
    Translation.Z += ((Translation.Z * 0.15f + 
        (FMath::Fmod(Translation.X, 5.0f) * 5.0f - 12.0f) + 
        (FMath::Fmod(Translation.Y, 5.0f) * 5.0f - 12.0f)) * VerticalOffset + StartHeight) * Alpha;
    float Angle = FMath::Atan2(Translation.Y, Translation.X);
    float Dist = FMath::Sqrt(Translation.X * Translation.X + Translation.Y * Translation.Y);
    Translation.X += FMath::Cos(Angle + (Alpha + OriginalAlpha) * PI * 12.0f) * Dist * HorizontalOffset * Alpha;
    Translation.Y += FMath::Sin(Angle + (Alpha + OriginalAlpha) * PI * 12.0f) * Dist * HorizontalOffset * Alpha;
    OutTransform.SetTranslation(Translation);
    OutTransform.SetScale3D(VoxelTransform.GetScale3D() * (1 + (Alpha + OriginalAlpha) * 2.0f));
}

void UVoxelComponent::OnComponentCreated()
{
    this->VoxelEffectDelegate.AddDynamic(this, &UVoxelComponent::ApplySpawnEffect);
	this->InitVoxel();
}
